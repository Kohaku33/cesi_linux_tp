# I - Utilisateurs

## 1 - Création et configuration

###	1 Utilisateur créé et configuré : 

`[luc@localhost ~]$ sudo useradd -m bob`

Avec l'option -m pour préciser que le répertoire home se trouve dans /home.

```
[luc@localhost etc]$ cat /etc/passwd
(...)
bob:x:1001:1001::/home/bob:/bin/bash
```

###	2 Groupe admins créé : 

```
[luc@localhost etc]$ sudo groupadd admins

[luc@localhost etc]$ cat /etc/group
(...)
admins:x:1002:bob
```

###	3 Groupe admins ajouté au fichier /etc/sudoers :

`[luc@localhost home]$ sudo visudo -f /etc/sudoers`

Appliquer les droits admins au groupe : 


```
## Allows people in group wheel to run all commands
%wheel  ALL=(ALL)       ALL
%admins ALL=(ALL)       ALL   
```


### 4 Ajouter l'utilisateur au groupe admins : 


```
[luc@localhost ~]$ sudo gpasswd -a bob admins
[sudo] Mot de passe de luc : 
Adding user bob to group admins

[luc@localhost etc]$ cat /etc/group
(...)
admins:x:1002:bob
```


##	2 - SSH 

```
PS C:\Users\Lucky\Documents\CESI\TP\RISR\Linux\GIT\folder_cesi\cesi_linux_tp> ssh luc@10.88.88.10
Enter passphrase for key 'C:\Users\Lucky/.ssh/id_rsa':
Last login: Thu Jan  7 09:38:40 2021 from 10.88.88.1
Last login: Thu Jan  7 09:38:40 2021 from 10.88.88.1
```

#	II Configuration réseau

##	1 Nom de domaine

```
[luc@localhost ~]$ sudo vim /etc/hostname
testdomain

sudo hostname testdomain
```
	
On restart et on verifi que le nom de domaine de la machine a bien changer :
 
```
[luc@testdomain ~]$ hostname
testdomain
```

##	2 Serveur DNS 

```
[luc@testdomain ~]$ sudo vim /etc/hosts
1.1.1.1 testdomain.lan testdomain

[luc@testdomain ~]$ ping testdomain.lan
PING testdomain.lan (1.1.1.1) 56(84) bytes of data.
64 bytes from testdomain.lan (1.1.1.1): icmp_seq=1 ttl=63 time=5.80 ms
```

#	III Partitionnement 

##	1 Préparation de la VM

```
[luc@testdomain ~]$ sudo pvcreate /dev/sdb
[sudo] Mot de passe de luc : 
  Physical volume "/dev/sdb" successfully created.
[luc@testdomain ~]$ sudo pvcreate /dev/sdc
  Physical volume "/dev/sdc" successfully created.
[luc@testdomain ~]$ man fdisk
[luc@testdomain ~]$ sudo pvs
  PV         VG     Fmt  Attr PSize  PFree
  /dev/sda2  centos lvm2 a--  <7,00g    0
  /dev/sdb          lvm2 ---   3,00g 3,00g
  /dev/sdc          lvm2 ---   3,00g 3,00g
```

##	2 Partitionnement 

* Agréger les deux disques en un seul volume group

```
[luc@testdomain ~]$ sudo vgcreate gdisk1 /dev/sdb /dev/sdc
  Volume group "gdisk1" successfully created
[luc@testdomain ~]$ sudo vgs
  VG     #PV #LV #SN Attr   VSize  VFree
  centos   1   2   0 wz--n- <7,00g    0
  gdisk1   2   0   0 wz--n-  5,99g 5,99g
  ```  

* Créer 3 logical volumes de 2 Go chacun : 

```
[luc@testdomain ~]$ sudo lvcreate -L 2G gdisk1 -n ldisk1
  Logical volume "ldisk1" created.
[luc@testdomain ~]$ sudo lvcreate -L 2G gdisk1 -n ldisk2
  Logical volume "ldisk2" created.
[luc@testdomain ~]$ sudo lvcreate -l 100%FREE gdisk1 -n ldisk3
  Logical volume "ldisk3" created.
[luc@testdomain ~]$ sudo lvs
  LV     VG     Attr       LSize   Pool Origin Data%  Meta%  Move Log Cpy%Sync Convert
  root   centos -wi-ao----  <6,20g                                                  
  swap   centos -wi-ao---- 820,00m                                                  
  ldisk1 gdisk1 -wi-a-----   2,00g                                                  
  ldisk2 gdisk1 -wi-a-----   2,00g                                                  
  ldisk3 gdisk1 -wi-a-----   1,99g 
```
     

* Formater les partitions en ext4 :

```
[luc@testdomain ~]$ sudo mkfs -t ext4 /dev/gdisk1/ldisk1
mke2fs 1.42.9 (28-Dec-2013)
Étiquette de système de fichiers=
Type de système d'exploitation : Linux
Taille de bloc=4096 (log=2)
Taille de fragment=4096 (log=2)
« Stride » = 0 blocs, « Stripe width » = 0 blocs
131072 i-noeuds, 524288 blocs
26214 blocs (5.00%) réservés pour le super utilisateur
Premier bloc de données=0
Nombre maximum de blocs du système de fichiers=536870912
16 groupes de blocs
32768 blocs par groupe, 32768 fragments par groupe
8192 i-noeuds par groupe
Superblocs de secours stockés sur les blocs :
        32768, 98304, 163840, 229376, 294912

Allocation des tables de groupe : complété
Écriture des tables d'i-noeuds : complété
Création du journal (16384 blocs) : complété
Écriture des superblocs et de l'information de comptabilité du système de
fichiers : complété

[luc@testdomain ~]$ sudo mkfs -t ext4 /dev/gdisk1/ldisk2

(...)

[luc@testdomain ~]$ sudo mkfs -t ext4 /dev/gdisk1/ldisk3

(...)

```

*	Monter les partitions : 

```
[luc@testdomain mnt]$ sudo kdir /mnt/part1
[luc@testdomain mnt]$ ls
part1

[luc@testdomain mnt]$ sudo mount /dev/gdisk1/ldisk1 /mnt/part1/
[luc@testdomain mnt]$ sudo mount /dev/gdisk1/ldisk2 /mnt/part2/
[luc@testdomain mnt]$ sudo mount /dev/gdisk1/ldisk3 /mnt/part3/

[luc@testdomain mnt]$ df -h
Sys. de fichiers          Taille Utilisé Dispo Uti% Monté sur
devtmpfs                    484M       0  484M   0% /dev
tmpfs                       496M       0  496M   0% /dev/shm
tmpfs                       496M    6,8M  489M   2% /run
tmpfs                       496M       0  496M   0% /sys/fs/cgroup
/dev/mapper/centos-root     6,2G    1,5G  4,8G  24% /
/dev/sda1                  1014M    168M  847M  17% /boot
tmpfs                       100M       0  100M   0% /run/user/1000
/dev/mapper/gdisk1-ldisk1   2,0G    6,0M  1,8G   1% /mnt/part1
/dev/mapper/gdisk1-ldisk2   2,0G    6,0M  1,8G   1% /mnt/part2
/dev/mapper/gdisk1-ldisk3   2,0G    6,0M  1,9G   1% /mnt/part3
```

##	Montage automatique des partition au démarrage
 
```
[luc@testdomain mnt]$ sudo vim /etc/fstab

/dev/gdisk1/ldisk1 /mtn/part1                   ext4    defaults        0 0
/dev/gdisk1/ldisk2 /mtn/part2                   ext4    defaults        0 0
/dev/gdisk1/ldisk3 /mtn/part3                   ext4    defaults        0 0
```

# IV Gestion de services 

## 	1 Interaction avec un service exsistant

S'assurer que l'unité est démarrée : 


```
[luc@testdomain ~]$ systemctl is-active firewalld
active
```

S'assurer que l'unité est activée 

```
[luc@testdomain ~]$ systemctl is-enabled firewalld
enabled
```

## 2 Création de service


### 	A. Unité simpliste

Création du ficher webservice :

`[luc@testdomain ~]$ sudo vim /etc/systemd/system/web.service`

Autoriser les connexion au port et de prendre en compte les changements :
```

[luc@testdomain ~]$ sudo firewall-cmd --add-port=80/tcp --permanent
success
[luc@testdomain ~]$ sudo firewall-cmd --reload
success
```

Une fois l'unité de service créée, il faut demander à systemd de relire les fichiers de configuration : 

```
[luc@testdomain ~]$ sudo systemctl daemon-reload
```

Enfin, on peut interagir avec notre unité : 

```
[luc@testdomain ~]$ sudo systemctl status web
● web.service - Very simple web service
   Loaded: loaded (/etc/systemd/system/web.service; disabled; vendor preset: disabled)
   Active: inactive (dead)
[luc@testdomain ~]$ sudo systemctl start web
[luc@testdomain ~]$ sudo systemctl enable web
Created symlink from /etc/systemd/system/multi-user.target.wants/web.service to /etc/systemd/system/web.service.
[luc@testdomain ~]$ sudo systemctl status web
● web.service - Very simple web service
   Loaded: loaded (/etc/systemd/system/web.service; enabled; vendor preset: disabled)
   Active: active (running) since mer. 2021-01-06 23:17:29 CET; 16s ago
 Main PID: 1502 (python2)
   CGroup: /system.slice/web.service
           └─1502 /bin/python2 -m SimpleHTTPServer 8888

janv. 06 23:17:29 testdomain systemd[1]: Started Very simple web service.
```

### 	B. Modification de l'unité 

```
[luc@testdomain ~]$ sudo adduser web

[luc@testdomain ~]$ sudo vim /etc/systemd/system/web.service
```

###	 Test du site :
```

[luc@testdomain srv]$ curl http://testdomain:8888/
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 3.2 Final//EN"><html>
<title>Directory listing for /</title>
<body>
<h2>Directory listing for /</h2>
<hr>
<ul>
<li><a href="bin/">bin@</a>
<li><a href="boot/">boot/</a>
<li><a href="dev/">dev/</a>
<li><a href="etc/">etc/</a>
<li><a href="home/">home/</a>
<li><a href="lib/">lib@</a>
<li><a href="lib64/">lib64@</a>
<li><a href="media/">media/</a>
<li><a href="mnt/">mnt/</a>
<li><a href="mtn/">mtn/</a>
<li><a href="opt/">opt/</a>
<li><a href="proc/">proc/</a>
<li><a href="root/">root/</a>
<li><a href="run/">run/</a>
<li><a href="sbin/">sbin@</a>
<li><a href="srv/">srv/</a>
<li><a href="sys/">sys/</a>
<li><a href="tmp/">tmp/</a>
<li><a href="usr/">usr/</a>
<li><a href="var/">var/</a>
</ul>
<hr>
</body>
</html>
```

# FIN











