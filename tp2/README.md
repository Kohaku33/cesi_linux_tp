
# I. Base de données

```
[luc@db ~]$ sudo yum install mariadb-server

[...]

Installé :
  mariadb-server.x86_64 1:5.5.68-1.el7

Dépendances installées :
  perl-Compress-Raw-Bzip2.x86_64 0:2.061-3.el7              perl-Compress-Raw-Zlib.x86_64 1:2.061-4.el7
  perl-DBD-MySQL.x86_64 0:4.023-6.el7                       perl-DBI.x86_64 0:1.627-4.el7
  perl-Data-Dumper.x86_64 0:2.145-3.el7                     perl-IO-Compress.noarch 0:2.061-2.el7
  perl-Net-Daemon.noarch 0:0.48-5.el7                       perl-PlRPC.noarch 0:0.2020-14.el7

Terminé !

```

* se connecter à la base

```
[luc@db ~]$ sudo mysql
Welcome to the MariaDB monitor.  Commands end with ; or \g.
Your MariaDB connection id is 4
Server version: 5.5.68-MariaDB MariaDB Server

Copyright (c) 2000, 2018, Oracle, MariaDB Corporation Ab and others.

Type 'help;' or '\h' for help. Type '\c' to clear the current input statement.
```

* créer une base

```
MariaDB [(none)]> CREATE DATABASE CESI;
Query OK, 1 row affected (0.00 sec)
```

* créer un utilisateur


```
MariaDB [(none)]> CREATE USER 'marie'@'10.99.99.1' IDENTIFIED BY '123';
Query OK, 0 rows affected (0.00 sec)

```


* attribuer les droits sur la base de données à l'utilisateur

```
MariaDB [(none)]> GRANT ALl PRIVILEGES ON CESI. * TO 'marie'@'10.99.99.11';
Query OK, 0 rows affected (0.00 sec)

MariaDB [(none)]> FLUSH PRIVILEGES;
Query OK, 0 rows affected (0.00 sec)
```
* ouvrir un port firewall pour que d'autres machines puissent accéder à la base

```
[luc@db ~]$ sudo ss -alnpt
[sudo] Mot de passe de luc : 
State       Recv-Q Send-Q             Local Address:Port                            Peer Address:Port
LISTEN      0      50                             *:3306                                       *:*                   users:(("mysqld",pid=2382,fd=14))
LISTEN      0      128                            *:22                                         *:*                   users:(("sshd",pid=1122,fd=3))
LISTEN      0      100                    127.0.0.1:25                                         *:*                   users:(("master",pid=1360,fd=13))
LISTEN      0      128                         [::]:22                                      [::]:*                   users:(("sshd",pid=1122,fd=4))
LISTEN      0      100                        [::1]:25                                      [::]:*                   users:(("master",pid=1360,fd=14))
[luc@db ~]$ sudo firewall-cmd --add-port=3306/tcp --permanent
success
[luc@db ~]$ sudo firewall-cmd --reload
success
[luc@db ~]$ sudo firewall-cmd --list-all
public (active)
  target: default
  icmp-block-inversion: no
  interfaces: enp0s3 enp0s8
  sources:
  services: dhcpv6-client ssh
  ports: 80/tcp 8888/tcp 3306/tcp
  protocols:
  masquerade: no
  forward-ports:
  source-ports:
  icmp-blocks:
  rich rules:
```


# II. Serveur Web


* installer le serveur Web (httpd) et le langage PHP (pour installer PHP, voir les instructions plus bas)

```
[sudo] Mot de passe de luc : 
Modules complémentaires chargés : fastestmirror
Loading mirror speeds from cached hostfile
[...]
Le paquet httpd-2.4.6-97.el7.centos.x86_64 est déjà installé dans sa dernière version
```

* télécharger wordpress

```

[luc@web ~]$ sudo yum install wget


Installé :
  wget.x86_64 0:1.14-18.el7_6.1

Terminé !

[luc@web ~]$ sudo wget https://wordpress.org/latest.tar.gz
--2021-01-08 09:47:03--  https://wordpress.org/latest.tar.gz
Résolution de wordpress.org (wordpress.org)... 198.143.164.252
Connexion vers wordpress.org (wordpress.org)|198.143.164.252|:443...connecté.
requête HTTP transmise, en attente de la réponse...200 OK
Longueur: 15422346 (15M) [application/octet-stream]
Sauvegarde en : «latest.tar.gz»

100%[===========================================================================>] 15 422 346  5,91MB/s   ds 2,5s

2021-01-08 09:47:06 (5,91 MB/s) - «latest.tar.gz» sauvegardé [15422346/15422346]


```

* extraire l'archive wordpress dans un dossier qui pourra être servi par Apache

```


tar xzvf latest.tar.gz
[luc@web ~]$ sudo yum install rsync
[luc@web ~]$ sudo rsync -avP ~/wordpress/ /var/www/html

[...]

sent 52,081,172 bytes  received 42,856 bytes  34,749,352.00 bytes/sec
total size is 51,918,380  speedup is 1.00

[luc@web ~]$  mkdir /var/www/html/wp-content/uploads
[luc@web ~]$ sudo chown -R apache:apache /var/www/html/*

```

* renseigner à Wordpress les informations qu'il utilisera pour se connecter à la base de * données (dans son fichier de configuration)

```
[luc@web html]$ sudo vim wp-config.php

define( 'DB_NAME', 'CESI' );

/** MySQL database username */
define( 'DB_USER', 'marie' );

/** MySQL database password */
define( 'DB_PASSWORD', '123' );

/** MySQL hostname */
define( 'DB_HOST', '10.99.99.12' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8' );

```

* configurer Apache pour qu'il serve le dossier où se trouve Wordpress

```

Apache sert déjà le dossier où se trouve Wordpress comme dit dans le fichier html

```

* lancer le serveur de base de donnée, puis le serveur web

```
[luc@db ~]$ sudo systemctl start mariadb
[sudo] Mot de passe de luc : 
[luc@db ~]$ sudo systemctl enable mariadb
[luc@db ~]$ sudo systemctl status mariadb
● mariadb.service - MariaDB database server
   Loaded: loaded (/usr/lib/systemd/system/mariadb.service; enabled; vendor preset: disabled)
   Active: active (running) since ven. 2021-01-08 09:24:29 CET; 1h 12min ago

[luc@web httpd]$ sudo systemctl start httpd
[sudo] Mot de passe de luc : 
[luc@web httpd]$ sudo systemctl enable httpd
Created symlink from /etc/systemd/system/multi-user.target.wants/httpd.service to /usr/lib/systemd/system/httpd.service.
[luc@web httpd]$ systemctl status httpd
● httpd.service - The Apache HTTP Server
   Loaded: loaded (/usr/lib/systemd/system/httpd.service; enabled; vendor preset: disabled)
   Active: active (running) since ven. 2021-01-08 10:32:00 CET; 4min 11s ago
```

* ouvrir le port firewall sur le serveur web

```

[luc@web httpd]$ sudo firewall-cmd --permanent --add-service=https
success
[luc@web httpd]$ sudo firewall-cmd --reload
success

```

* accéder à l'interface de Wordpress afin de valider la bonne installation de la solution


```

Le serveur fonctionne. La page s'affiche correctement sur le navigateur.

```


# III. Reverse Proxy


```



```

#IV. Un peu de sécu

```



```

## 1. fail2ban

```



```

## 2. HTTPS

```



```

### Présentation

```



```

### A. Génération du certificat et de la clé privée associée

```



```

### B. Configurer le reverse proxy


```



```


## 3. Monitoring

```



```

### Présentation

```



```

### A. Installation de Netdata

```



```

### B. Alerting

```



```

### C. Bonus

```



```